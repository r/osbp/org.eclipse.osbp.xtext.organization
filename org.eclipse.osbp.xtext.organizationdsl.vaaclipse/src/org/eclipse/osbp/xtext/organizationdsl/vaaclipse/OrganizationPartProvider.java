/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.organizationdsl.vaaclipse;

import org.eclipse.osbp.ui.api.organization.IOrganizationViewPartProvider;
import org.osgi.service.component.annotations.Component;

@Component(property={"service.ranking:Integer=0"})
public class OrganizationPartProvider implements IOrganizationViewPartProvider {

	@Override
	public String getViewPartURI() {
		return "bundleclass://org.eclipse.osbp.xtext.organizationdsl.vaaclipse/org.eclipse.osbp.xtext.organizationdsl.vaaclipse.OSBPOrganizationPart";
	}

}
