/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.organizationdsl.vaaclipse;

import java.util.Locale;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.osbp.osgi.hybrid.api.AbstractHybridVaaclipseView;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.slf4j.Logger;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("all")
public class OSBPOrganizationPart extends AbstractHybridVaaclipseView implements
		IUser.UserLocaleListener {
	private static Logger log = org.slf4j.LoggerFactory
			.getLogger("organizations");

	@Inject
	public OSBPOrganizationPart(final VerticalLayout parent,
			final IEclipseContext context, final MApplication app) {
		super(parent, context, app);
	}

	public void createView(final VerticalLayout parent) {
		parent.addComponent(new Label(
				"No implementation available for OSBP open source."));
	}

	public void createComponents() {

	}

	@Override
	public void localeChanged(final Locale locale) {

	}
}
