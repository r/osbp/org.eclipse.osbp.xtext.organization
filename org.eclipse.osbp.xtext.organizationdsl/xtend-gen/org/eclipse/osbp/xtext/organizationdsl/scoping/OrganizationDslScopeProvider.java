/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.organizationdsl.scoping;

import com.google.common.base.Objects;
import java.util.ArrayList;
import javax.inject.Inject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.xtext.datamartdsl.jvmmodel.DatamartDSLJvmModelInferrer;
import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDslUtils;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior;
import org.eclipse.osbp.xtext.organizationdsl.scoping.AbstractOrganizationDslScopeProvider;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it
 */
@SuppressWarnings("all")
public class OrganizationDslScopeProvider extends AbstractOrganizationDslScopeProvider {
  @Inject
  @Extension
  private DatamartDSLJvmModelInferrer datamartInferrer;
  
  @Inject
  @Extension
  private OrganizationDslUtils orgUtils;
  
  @Override
  public IScope getScope(final EObject context, final EReference reference) {
    boolean _equals = Objects.equal(reference, OrganizationDSLPackage.Literals.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE);
    if (_equals) {
      return this.getScope_Organization_Superior_Superior(((OrganizationSuperior) context));
    } else {
      boolean _equals_1 = Objects.equal(reference, OrganizationDSLPackage.Literals.ORGANIZATION__SUPERIOR_POS_VALUE);
      if (_equals_1) {
        return this.getScope_Organization_SuperiorPosValue(((Organization) context));
      } else {
        return super.getScope(context, reference);
      }
    }
  }
  
  public IScope getScope_Organization_SuperiorPosValue(final Organization organization) {
    ArrayList<EObject> result = CollectionLiterals.<EObject>newArrayList();
    EObject eObj = organization.eContainer();
    while ((!(eObj instanceof Organization))) {
      eObj = eObj.eContainer();
    }
    boolean _notEquals = (!Objects.equal(eObj, null));
    if (_notEquals) {
      EList<OrganizationPosition> _organizationPositions = ((Organization) eObj).getOrganizationPositions();
      for (final OrganizationPosition pos : _organizationPositions) {
        result.add(pos);
      }
      return Scopes.scopeFor(result);
    }
    return null;
  }
  
  public IScope getScope_Organization_Superior_Superior(final OrganizationSuperior orgSup) {
    ArrayList<EObject> result = CollectionLiterals.<EObject>newArrayList();
    OrganizationPosition orgSupPosition = null;
    EObject eObj = orgSup.eContainer();
    while ((!(eObj instanceof Organization))) {
      {
        if ((eObj instanceof OrganizationPosition)) {
          orgSupPosition = ((OrganizationPosition) eObj);
        }
        eObj = eObj.eContainer();
      }
    }
    boolean _notEquals = (!Objects.equal(eObj, null));
    if (_notEquals) {
      EList<OrganizationPosition> _organizationPositions = ((Organization) eObj).getOrganizationPositions();
      for (final OrganizationPosition pos : _organizationPositions) {
        if ((((!Objects.equal(pos.getName(), null)) && (!Objects.equal(orgSupPosition, null))) && (!pos.getName().equals(orgSupPosition.getName())))) {
          result.add(pos);
        }
      }
      return Scopes.scopeFor(result);
    }
    return null;
  }
}
