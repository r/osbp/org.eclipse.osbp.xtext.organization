/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.organizationdsl;

import javax.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function0;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class OrganizationDslUtils {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  public final String MX_GRAPH_PREFIX = "MxGraph";
  
  public final String ORG_TXT = "Organization";
  
  public final String JS_TXT = "Js";
  
  public final String STATE_SUFFIX = "State";
  
  public final String ORG_SUFFIX = "Organization";
  
  public final String POS_SUFFIX = "Position";
  
  public final String MX_GRAPH_ORG_CHART_VIEW_ID = new Function0<String>() {
    public String apply() {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(OrganizationDslUtils.this.MX_GRAPH_PREFIX);
      _builder.append(OrganizationDslUtils.this.ORG_TXT);
      return _builder.toString();
    }
  }.apply();
  
  public final String MX_GRAPH_ORG_CHART_ID = new Function0<String>() {
    public String apply() {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(OrganizationDslUtils.this.MX_GRAPH_PREFIX);
      _builder.append(OrganizationDslUtils.this.ORG_TXT);
      _builder.append(OrganizationDslUtils.this.JS_TXT);
      return _builder.toString();
    }
  }.apply();
  
  public final String MX_GRAPH_ORG_CHART_STATE_ID = new Function0<String>() {
    public String apply() {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(OrganizationDslUtils.this.MX_GRAPH_PREFIX);
      _builder.append(OrganizationDslUtils.this.ORG_TXT);
      _builder.append(OrganizationDslUtils.this.JS_TXT);
      _builder.append(OrganizationDslUtils.this.STATE_SUFFIX);
      return _builder.toString();
    }
  }.apply();
  
  public String i18nKey(final EObject obj) {
    return this._iQualifiedNameProvider.getFullyQualifiedName(obj).toString();
  }
  
  private String createFilename(final Organization organization) {
    String _name = null;
    if (organization!=null) {
      _name=organization.getName();
    }
    String _string = null;
    if (_name!=null) {
      _string=_name.toString();
    }
    return _string;
  }
  
  public String createJsChartFilename(final Organization organization) {
    String _createFilename = null;
    if (organization!=null) {
      _createFilename=this.createFilename(organization);
    }
    String _firstUpper = null;
    if (_createFilename!=null) {
      _firstUpper=StringExtensions.toFirstUpper(_createFilename);
    }
    return _firstUpper.concat(this.MX_GRAPH_ORG_CHART_ID);
  }
  
  public String createfullyQualifiedJsChartFilename(final Organization organization) {
    String _fullyQualifiedNameToFirstUpper = null;
    if (organization!=null) {
      _fullyQualifiedNameToFirstUpper=this.fullyQualifiedNameToFirstUpper(organization);
    }
    String _string = null;
    if (_fullyQualifiedNameToFirstUpper!=null) {
      _string=_fullyQualifiedNameToFirstUpper.toString();
    }
    String _concat = null;
    if (_string!=null) {
      _concat=_string.concat(this.MX_GRAPH_ORG_CHART_ID);
    }
    return _concat;
  }
  
  public String createJsFilename(final Organization organization) {
    String _createFilename = null;
    if (organization!=null) {
      _createFilename=this.createFilename(organization);
    }
    String _concat = null;
    if (_createFilename!=null) {
      _concat=_createFilename.concat("MxGraphOrg.js");
    }
    return _concat;
  }
  
  public String createJClassJsFilename(final Organization organization) {
    String _createFilename = null;
    if (organization!=null) {
      _createFilename=this.createFilename(organization);
    }
    String _concat = null;
    if (_createFilename!=null) {
      _concat=_createFilename.concat("MxGraphOrgJClass.js");
    }
    return _concat;
  }
  
  public String createfullyQualifiedJsStateFilename(final Organization organization) {
    String _fullyQualifiedNameToFirstUpper = null;
    if (organization!=null) {
      _fullyQualifiedNameToFirstUpper=this.fullyQualifiedNameToFirstUpper(organization);
    }
    String _string = null;
    if (_fullyQualifiedNameToFirstUpper!=null) {
      _string=_fullyQualifiedNameToFirstUpper.toString();
    }
    String _concat = null;
    if (_string!=null) {
      _concat=_string.concat(this.MX_GRAPH_ORG_CHART_STATE_ID);
    }
    return _concat;
  }
  
  public String createStateFilename(final Organization organization) {
    String _createFilename = null;
    if (organization!=null) {
      _createFilename=this.createFilename(organization);
    }
    String _firstUpper = null;
    if (_createFilename!=null) {
      _firstUpper=StringExtensions.toFirstUpper(_createFilename);
    }
    return _firstUpper.concat(this.MX_GRAPH_ORG_CHART_STATE_ID);
  }
  
  public String createViewFilename(final Organization organization) {
    String _createFilename = null;
    if (organization!=null) {
      _createFilename=this.createFilename(organization);
    }
    String _firstUpper = null;
    if (_createFilename!=null) {
      _firstUpper=StringExtensions.toFirstUpper(_createFilename);
    }
    return _firstUpper.concat(this.MX_GRAPH_ORG_CHART_VIEW_ID);
  }
  
  public String createResourceBundlePath(final Organization organization) {
    String _createFilename = null;
    if (organization!=null) {
      _createFilename=this.createFilename(organization);
    }
    String _concat = null;
    if (_createFilename!=null) {
      _concat=_createFilename.concat("ResourceBundle");
    }
    return _concat;
  }
  
  public String createResBundleFilename(final Organization organization) {
    String _createFilename = null;
    if (organization!=null) {
      _createFilename=this.createFilename(organization);
    }
    String _concat = null;
    if (_createFilename!=null) {
      _concat=_createFilename.concat("ResourceBundle.properties");
    }
    return _concat;
  }
  
  public String createOrganizationFilename(final Organization organization) {
    String _name = null;
    if (organization!=null) {
      _name=organization.getName();
    }
    return StringExtensions.toFirstUpper(_name).concat(this.ORG_SUFFIX);
  }
  
  public String createPositionFilename(final OrganizationPosition position) {
    return StringExtensions.toFirstUpper(position.getName()).concat(this.POS_SUFFIX);
  }
  
  public String fullyQualifiedNameToFirstUpper(final Organization org) {
    String _xblockexpression = null;
    {
      QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(org);
      String _string = null;
      if (_fullyQualifiedName!=null) {
        _string=_fullyQualifiedName.toString();
      }
      String fqn = _string;
      int lastIdxPoint = fqn.lastIndexOf(".");
      if ((lastIdxPoint > 0)) {
        String firstPartStr = fqn.substring(0, lastIdxPoint);
        String orgName = fqn.substring((lastIdxPoint + 1));
        String orgNameFU = StringExtensions.toFirstUpper(orgName);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(firstPartStr);
        _builder.append(".");
        _builder.append(orgNameFU);
        fqn = _builder.toString();
      } else {
        String _createFilename = null;
        if (org!=null) {
          _createFilename=this.createFilename(org);
        }
        fqn = StringExtensions.toFirstUpper(_createFilename);
      }
      _xblockexpression = fqn;
    }
    return _xblockexpression;
  }
}
