/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.organizationdsl.validation;

import com.google.common.base.Objects;
import java.util.ArrayList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior;
import org.eclipse.osbp.xtext.organizationdsl.validation.AbstractOrganizationDslValidator;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class OrganizationDslValidator extends AbstractOrganizationDslValidator {
  /**
   * The first position is not allowed to have a superior position (superiorPos)
   */
  @Check
  public void checkOrganizationFirstPositionSuperiorPos(final Organization org) {
    boolean _isEmpty = org.getOrganizationPositions().isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      final OrganizationPosition organizationPosition = org.getOrganizationPositions().get(0);
      boolean _notEquals = (!Objects.equal(organizationPosition, null));
      if (_notEquals) {
        final OrganizationSuperior superior = organizationPosition.getSuperior();
        if (((!Objects.equal(superior, null)) && (!Objects.equal(superior.getSuperiorPosValue(), null)))) {
          this.error("First position only without superiorPos reference allowed", organizationPosition, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR);
        }
      }
    }
  }
  
  @Check
  public void checkOrganizationNamesAndSuperiorPos(final Organization org) {
    this.checkOrganizationNamesAndSuperiorPos(org, CollectionLiterals.<String>newArrayList());
  }
  
  /**
   * validation that only one position exist without a superPos reference within each organisation
   * and that no duplicate organization name is used.
   */
  private void checkOrganizationPositionNamesAndSuperiorPos(final Organization org) {
    ArrayList<String> posNames = CollectionLiterals.<String>newArrayList();
    int superiorPos = 0;
    EList<OrganizationPosition> _organizationPositions = org.getOrganizationPositions();
    for (final OrganizationPosition pos : _organizationPositions) {
      {
        boolean _contains = posNames.contains(pos.getName());
        if (_contains) {
          this.error("Duplicate position name: ".concat(pos.getName()), pos, OrganizationDSLPackage.Literals.ORGANIZATION_BASE__NAME);
        } else {
          posNames.add(pos.getName());
        }
        final OrganizationSuperior superior = pos.getSuperior();
        boolean _notEquals = (!Objects.equal(superior, null));
        if (_notEquals) {
          OrganizationPosition _superiorPosValue = superior.getSuperiorPosValue();
          boolean _equals = Objects.equal(_superiorPosValue, null);
          if (_equals) {
            this.error("Please include superiorPos value", pos, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR);
          } else {
            if (((!Objects.equal(pos.getName(), null)) && pos.getName().equals(superior.getSuperiorPosValue().getName()))) {
              this.error("Please do not use the same position as superiorPos (endless loop)", pos, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR);
            }
          }
        }
        if ((Objects.equal(superior, null) || ((!Objects.equal(superior, null)) && Objects.equal(superior.getSuperiorPosValue(), null)))) {
          if ((superiorPos > 0)) {
            this.error("Only one position without superiorPos reference allowed", pos, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR);
          } else {
            superiorPos = (superiorPos + 1);
          }
        }
      }
    }
  }
  
  /**
   * validation that only one position exist without a superior position (superiorPos) reference within each organization
   * and that no duplicate organization name is used.
   */
  private void checkOrganizationNamesAndSuperiorPos(final Organization org, final ArrayList<String> orgNames) {
    boolean _notEquals = (!Objects.equal(org, null));
    if (_notEquals) {
      boolean _contains = orgNames.contains(org.getName());
      if (_contains) {
        this.error("Duplicate organization name: ".concat(org.getName()), org, OrganizationDSLPackage.Literals.ORGANIZATION_BASE__NAME);
      } else {
        orgNames.add(org.getName());
      }
      this.checkOrganizationPositionNamesAndSuperiorPos(org);
      EList<Organization> _subOrganizations = org.getSubOrganizations();
      for (final Organization subOrg : _subOrganizations) {
        this.checkOrganizationNamesAndSuperiorPos(subOrg, orgNames);
      }
    }
  }
}
