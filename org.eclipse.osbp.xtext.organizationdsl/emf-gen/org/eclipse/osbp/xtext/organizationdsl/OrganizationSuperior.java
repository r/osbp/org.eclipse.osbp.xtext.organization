/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Organization Superior</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior#getSuperiorPosValue <em>Superior Pos Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganizationSuperior()
 * @model
 * @generated
 */
public interface OrganizationSuperior extends OrganizationLazyResolver {
	/**
	 * Returns the value of the '<em><b>Superior Pos Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Superior Pos Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superior Pos Value</em>' reference.
	 * @see #setSuperiorPosValue(OrganizationPosition)
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganizationSuperior_SuperiorPosValue()
	 * @model
	 * @generated
	 */
	OrganizationPosition getSuperiorPosValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior#getSuperiorPosValue <em>Superior Pos Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Superior Pos Value</em>' reference.
	 * @see #getSuperiorPosValue()
	 * @generated
	 */
	void setSuperiorPosValue(OrganizationPosition value);

} // OrganizationSuperior
