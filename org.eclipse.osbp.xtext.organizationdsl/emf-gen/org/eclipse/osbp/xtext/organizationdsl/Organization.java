/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.action.ActionToolbar;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Organization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getSuperiorPosValue <em>Superior Pos Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getToolbar <em>Toolbar</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getOrganizationPositions <em>Organization Positions</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getSubOrganizations <em>Sub Organizations</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganization()
 * @model
 * @generated
 */
public interface Organization extends OrganizationBase {
	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Title</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganization_Title()
	 * @model unique="false"
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Superior Pos Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Superior Pos Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superior Pos Value</em>' reference.
	 * @see #setSuperiorPosValue(OrganizationPosition)
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganization_SuperiorPosValue()
	 * @model
	 * @generated
	 */
	OrganizationPosition getSuperiorPosValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getSuperiorPosValue <em>Superior Pos Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Superior Pos Value</em>' reference.
	 * @see #getSuperiorPosValue()
	 * @generated
	 */
	void setSuperiorPosValue(OrganizationPosition value);

	/**
	 * Returns the value of the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Toolbar</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Toolbar</em>' reference.
	 * @see #setToolbar(ActionToolbar)
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganization_Toolbar()
	 * @model
	 * @generated
	 */
	ActionToolbar getToolbar();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getToolbar <em>Toolbar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Toolbar</em>' reference.
	 * @see #getToolbar()
	 * @generated
	 */
	void setToolbar(ActionToolbar value);

	/**
	 * Returns the value of the '<em><b>Organization Positions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Organization Positions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Organization Positions</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganization_OrganizationPositions()
	 * @model containment="true"
	 * @generated
	 */
	EList<OrganizationPosition> getOrganizationPositions();

	/**
	 * Returns the value of the '<em><b>Sub Organizations</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.organizationdsl.Organization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Organizations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Organizations</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganization_SubOrganizations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Organization> getSubOrganizations();

} // Organization
