/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel fileExtensions='organization' modelName='OrganizationDSL' prefix='OrganizationDSL' updateClasspath='false' loadInitialization='false' literalsInterface='true' copyrightText='Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)\r\n All rights reserved. This program and the accompanying materials \r\n are made available under the terms of the Eclipse Public License 2.0  \r\n which accompanies this distribution, and is available at \r\n https://www.eclipse.org/legal/epl-2.0/ \r\n \r\n SPDX-License-Identifier: EPL-2.0 \r\n\r\n Based on ideas from Xtext, Xtend, Xcore\r\n   \r\n Contributors:  \r\n \t\tJose C. Dominguez - Initial implementation \r\n ' basePackage='org.eclipse.osbp.xtext'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore rootPackage='organizationDSL'"
 * @generated
 */
public interface OrganizationDSLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "organizationdsl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/xtext/organizationdsl/OrganizationDsl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "organizationDSL";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OrganizationDSLPackage eINSTANCE = org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationModelImpl <em>Organization Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationModelImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationModel()
	 * @generated
	 */
	int ORGANIZATION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_MODEL__IMPORT_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Pckg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_MODEL__PCKG = 1;

	/**
	 * The number of structural features of the '<em>Organization Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Organization Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationLazyResolverImpl <em>Organization Lazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationLazyResolverImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationLazyResolver()
	 * @generated
	 */
	int ORGANIZATION_LAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>Organization Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_LAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = 0;

	/**
	 * The number of operations of the '<em>Organization Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_LAZY_RESOLVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPackageImpl <em>Organization Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPackageImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationPackage()
	 * @generated
	 */
	int ORGANIZATION_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_PACKAGE__NAME = OSBPTypesPackage.LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Organizations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_PACKAGE__ORGANIZATIONS = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Organization Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_PACKAGE_FEATURE_COUNT = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_PACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Organization Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_PACKAGE_OPERATION_COUNT = OSBPTypesPackage.LPACKAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationBaseImpl <em>Organization Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationBaseImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationBase()
	 * @generated
	 */
	int ORGANIZATION_BASE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_BASE__NAME = ORGANIZATION_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Organization Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_BASE_FEATURE_COUNT = ORGANIZATION_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_BASE___ERESOLVE_PROXY__INTERNALEOBJECT = ORGANIZATION_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Organization Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_BASE_OPERATION_COUNT = ORGANIZATION_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl <em>Organization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganization()
	 * @generated
	 */
	int ORGANIZATION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__NAME = ORGANIZATION_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__TITLE = ORGANIZATION_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Superior Pos Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__SUPERIOR_POS_VALUE = ORGANIZATION_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__TOOLBAR = ORGANIZATION_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Organization Positions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__ORGANIZATION_POSITIONS = ORGANIZATION_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Sub Organizations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__SUB_ORGANIZATIONS = ORGANIZATION_BASE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_FEATURE_COUNT = ORGANIZATION_BASE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION___ERESOLVE_PROXY__INTERNALEOBJECT = ORGANIZATION_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_OPERATION_COUNT = ORGANIZATION_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPositionImpl <em>Organization Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPositionImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationPosition()
	 * @generated
	 */
	int ORGANIZATION_POSITION = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_POSITION__NAME = ORGANIZATION_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Link Alias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_POSITION__LINK_ALIAS = ORGANIZATION_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Superior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_POSITION__SUPERIOR = ORGANIZATION_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_POSITION__ROLES = ORGANIZATION_BASE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Organization Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_POSITION_FEATURE_COUNT = ORGANIZATION_BASE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_POSITION___ERESOLVE_PROXY__INTERNALEOBJECT = ORGANIZATION_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Organization Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_POSITION_OPERATION_COUNT = ORGANIZATION_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationSuperiorImpl <em>Organization Superior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationSuperiorImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationSuperior()
	 * @generated
	 */
	int ORGANIZATION_SUPERIOR = 6;

	/**
	 * The feature id for the '<em><b>Superior Pos Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE = ORGANIZATION_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Organization Superior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_SUPERIOR_FEATURE_COUNT = ORGANIZATION_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_SUPERIOR___ERESOLVE_PROXY__INTERNALEOBJECT = ORGANIZATION_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Organization Superior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_SUPERIOR_OPERATION_COUNT = ORGANIZATION_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationRoleImpl <em>Organization Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationRoleImpl
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationRole()
	 * @generated
	 */
	int ORGANIZATION_ROLE = 7;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_ROLE__ROLE = ORGANIZATION_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Organization Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_ROLE_FEATURE_COUNT = ORGANIZATION_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_ROLE___ERESOLVE_PROXY__INTERNALEOBJECT = ORGANIZATION_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Organization Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_ROLE_OPERATION_COUNT = ORGANIZATION_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 8;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationModel <em>Organization Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization Model</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationModel
	 * @generated
	 */
	EClass getOrganizationModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationModel#getImportSection()
	 * @see #getOrganizationModel()
	 * @generated
	 */
	EReference getOrganizationModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationModel#getPckg <em>Pckg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pckg</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationModel#getPckg()
	 * @see #getOrganizationModel()
	 * @generated
	 */
	EReference getOrganizationModel_Pckg();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationLazyResolver <em>Organization Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization Lazy Resolver</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationLazyResolver
	 * @generated
	 */
	EClass getOrganizationLazyResolver();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject) <em>EResolve Proxy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>EResolve Proxy</em>' operation.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject)
	 * @generated
	 */
	EOperation getOrganizationLazyResolver__EResolveProxy__InternalEObject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage <em>Organization Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization Package</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage
	 * @generated
	 */
	EClass getOrganizationPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage#getOrganizations <em>Organizations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Organizations</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage#getOrganizations()
	 * @see #getOrganizationPackage()
	 * @generated
	 */
	EReference getOrganizationPackage_Organizations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationBase <em>Organization Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization Base</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationBase
	 * @generated
	 */
	EClass getOrganizationBase();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationBase#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationBase#getName()
	 * @see #getOrganizationBase()
	 * @generated
	 */
	EAttribute getOrganizationBase_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.Organization <em>Organization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.Organization
	 * @generated
	 */
	EClass getOrganization();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.Organization#getTitle()
	 * @see #getOrganization()
	 * @generated
	 */
	EAttribute getOrganization_Title();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getSuperiorPosValue <em>Superior Pos Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Superior Pos Value</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.Organization#getSuperiorPosValue()
	 * @see #getOrganization()
	 * @generated
	 */
	EReference getOrganization_SuperiorPosValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getToolbar <em>Toolbar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Toolbar</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.Organization#getToolbar()
	 * @see #getOrganization()
	 * @generated
	 */
	EReference getOrganization_Toolbar();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getOrganizationPositions <em>Organization Positions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Organization Positions</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.Organization#getOrganizationPositions()
	 * @see #getOrganization()
	 * @generated
	 */
	EReference getOrganization_OrganizationPositions();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.organizationdsl.Organization#getSubOrganizations <em>Sub Organizations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Organizations</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.Organization#getSubOrganizations()
	 * @see #getOrganization()
	 * @generated
	 */
	EReference getOrganization_SubOrganizations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition <em>Organization Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization Position</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition
	 * @generated
	 */
	EClass getOrganizationPosition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getLinkAlias <em>Link Alias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Link Alias</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getLinkAlias()
	 * @see #getOrganizationPosition()
	 * @generated
	 */
	EAttribute getOrganizationPosition_LinkAlias();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getSuperior <em>Superior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Superior</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getSuperior()
	 * @see #getOrganizationPosition()
	 * @generated
	 */
	EReference getOrganizationPosition_Superior();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Roles</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getRoles()
	 * @see #getOrganizationPosition()
	 * @generated
	 */
	EReference getOrganizationPosition_Roles();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior <em>Organization Superior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization Superior</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior
	 * @generated
	 */
	EClass getOrganizationSuperior();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior#getSuperiorPosValue <em>Superior Pos Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Superior Pos Value</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior#getSuperiorPosValue()
	 * @see #getOrganizationSuperior()
	 * @generated
	 */
	EReference getOrganizationSuperior_SuperiorPosValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationRole <em>Organization Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization Role</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationRole
	 * @generated
	 */
	EClass getOrganizationRole();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationRole#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationRole#getRole()
	 * @see #getOrganizationRole()
	 * @generated
	 */
	EReference getOrganizationRole_Role();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @model instanceClass="org.eclipse.emf.ecore.InternalEObject"
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OrganizationDSLFactory getOrganizationDSLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationModelImpl <em>Organization Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationModelImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationModel()
		 * @generated
		 */
		EClass ORGANIZATION_MODEL = eINSTANCE.getOrganizationModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION_MODEL__IMPORT_SECTION = eINSTANCE.getOrganizationModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Pckg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION_MODEL__PCKG = eINSTANCE.getOrganizationModel_Pckg();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationLazyResolverImpl <em>Organization Lazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationLazyResolverImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationLazyResolver()
		 * @generated
		 */
		EClass ORGANIZATION_LAZY_RESOLVER = eINSTANCE.getOrganizationLazyResolver();

		/**
		 * The meta object literal for the '<em><b>EResolve Proxy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ORGANIZATION_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = eINSTANCE.getOrganizationLazyResolver__EResolveProxy__InternalEObject();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPackageImpl <em>Organization Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPackageImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationPackage()
		 * @generated
		 */
		EClass ORGANIZATION_PACKAGE = eINSTANCE.getOrganizationPackage();

		/**
		 * The meta object literal for the '<em><b>Organizations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION_PACKAGE__ORGANIZATIONS = eINSTANCE.getOrganizationPackage_Organizations();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationBaseImpl <em>Organization Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationBaseImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationBase()
		 * @generated
		 */
		EClass ORGANIZATION_BASE = eINSTANCE.getOrganizationBase();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANIZATION_BASE__NAME = eINSTANCE.getOrganizationBase_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl <em>Organization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganization()
		 * @generated
		 */
		EClass ORGANIZATION = eINSTANCE.getOrganization();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANIZATION__TITLE = eINSTANCE.getOrganization_Title();

		/**
		 * The meta object literal for the '<em><b>Superior Pos Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION__SUPERIOR_POS_VALUE = eINSTANCE.getOrganization_SuperiorPosValue();

		/**
		 * The meta object literal for the '<em><b>Toolbar</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION__TOOLBAR = eINSTANCE.getOrganization_Toolbar();

		/**
		 * The meta object literal for the '<em><b>Organization Positions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION__ORGANIZATION_POSITIONS = eINSTANCE.getOrganization_OrganizationPositions();

		/**
		 * The meta object literal for the '<em><b>Sub Organizations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION__SUB_ORGANIZATIONS = eINSTANCE.getOrganization_SubOrganizations();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPositionImpl <em>Organization Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPositionImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationPosition()
		 * @generated
		 */
		EClass ORGANIZATION_POSITION = eINSTANCE.getOrganizationPosition();

		/**
		 * The meta object literal for the '<em><b>Link Alias</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANIZATION_POSITION__LINK_ALIAS = eINSTANCE.getOrganizationPosition_LinkAlias();

		/**
		 * The meta object literal for the '<em><b>Superior</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION_POSITION__SUPERIOR = eINSTANCE.getOrganizationPosition_Superior();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION_POSITION__ROLES = eINSTANCE.getOrganizationPosition_Roles();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationSuperiorImpl <em>Organization Superior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationSuperiorImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationSuperior()
		 * @generated
		 */
		EClass ORGANIZATION_SUPERIOR = eINSTANCE.getOrganizationSuperior();

		/**
		 * The meta object literal for the '<em><b>Superior Pos Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE = eINSTANCE.getOrganizationSuperior_SuperiorPosValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationRoleImpl <em>Organization Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationRoleImpl
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getOrganizationRole()
		 * @generated
		 */
		EClass ORGANIZATION_ROLE = eINSTANCE.getOrganizationRole();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION_ROLE__ROLE = eINSTANCE.getOrganizationRole_Role();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //OrganizationDSLPackage
