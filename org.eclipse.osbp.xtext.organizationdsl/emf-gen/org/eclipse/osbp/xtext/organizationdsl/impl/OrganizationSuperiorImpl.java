/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Organization Superior</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationSuperiorImpl#getSuperiorPosValue <em>Superior Pos Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OrganizationSuperiorImpl extends OrganizationLazyResolverImpl implements OrganizationSuperior {
	/**
	 * The cached value of the '{@link #getSuperiorPosValue() <em>Superior Pos Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperiorPosValue()
	 * @generated
	 * @ordered
	 */
	protected OrganizationPosition superiorPosValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrganizationSuperiorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganizationDSLPackage.Literals.ORGANIZATION_SUPERIOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganizationPosition getSuperiorPosValue() {
		if (superiorPosValue != null && superiorPosValue.eIsProxy()) {
			InternalEObject oldSuperiorPosValue = (InternalEObject)superiorPosValue;
			superiorPosValue = (OrganizationPosition)eResolveProxy(oldSuperiorPosValue);
			if (superiorPosValue != oldSuperiorPosValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganizationDSLPackage.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE, oldSuperiorPosValue, superiorPosValue));
			}
		}
		return superiorPosValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganizationPosition basicGetSuperiorPosValue() {
		return superiorPosValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperiorPosValue(OrganizationPosition newSuperiorPosValue) {
		OrganizationPosition oldSuperiorPosValue = superiorPosValue;
		superiorPosValue = newSuperiorPosValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganizationDSLPackage.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE, oldSuperiorPosValue, superiorPosValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE:
				if (resolve) return getSuperiorPosValue();
				return basicGetSuperiorPosValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE:
				setSuperiorPosValue((OrganizationPosition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE:
				setSuperiorPosValue((OrganizationPosition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE:
				return superiorPosValue != null;
		}
		return super.eIsSet(featureID);
	}

} //OrganizationSuperiorImpl
