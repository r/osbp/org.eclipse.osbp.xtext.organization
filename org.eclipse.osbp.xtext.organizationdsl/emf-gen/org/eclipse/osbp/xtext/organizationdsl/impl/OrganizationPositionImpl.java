/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationRole;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Organization Position</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPositionImpl#getLinkAlias <em>Link Alias</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPositionImpl#getSuperior <em>Superior</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationPositionImpl#getRoles <em>Roles</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OrganizationPositionImpl extends OrganizationBaseImpl implements OrganizationPosition {
	/**
	 * The default value of the '{@link #getLinkAlias() <em>Link Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkAlias()
	 * @generated
	 * @ordered
	 */
	protected static final String LINK_ALIAS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLinkAlias() <em>Link Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkAlias()
	 * @generated
	 * @ordered
	 */
	protected String linkAlias = LINK_ALIAS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuperior() <em>Superior</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperior()
	 * @generated
	 * @ordered
	 */
	protected OrganizationSuperior superior;

	/**
	 * The cached value of the '{@link #getRoles() <em>Roles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<OrganizationRole> roles;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrganizationPositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganizationDSLPackage.Literals.ORGANIZATION_POSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLinkAlias() {
		return linkAlias;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkAlias(String newLinkAlias) {
		String oldLinkAlias = linkAlias;
		linkAlias = newLinkAlias;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganizationDSLPackage.ORGANIZATION_POSITION__LINK_ALIAS, oldLinkAlias, linkAlias));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganizationSuperior getSuperior() {
		return superior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuperior(OrganizationSuperior newSuperior, NotificationChain msgs) {
		OrganizationSuperior oldSuperior = superior;
		superior = newSuperior;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR, oldSuperior, newSuperior);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperior(OrganizationSuperior newSuperior) {
		if (newSuperior != superior) {
			NotificationChain msgs = null;
			if (superior != null)
				msgs = ((InternalEObject)superior).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR, null, msgs);
			if (newSuperior != null)
				msgs = ((InternalEObject)newSuperior).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR, null, msgs);
			msgs = basicSetSuperior(newSuperior, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR, newSuperior, newSuperior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrganizationRole> getRoles() {
		if (roles == null) {
			roles = new EObjectContainmentEList<OrganizationRole>(OrganizationRole.class, this, OrganizationDSLPackage.ORGANIZATION_POSITION__ROLES);
		}
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR:
				return basicSetSuperior(null, msgs);
			case OrganizationDSLPackage.ORGANIZATION_POSITION__ROLES:
				return ((InternalEList<?>)getRoles()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_POSITION__LINK_ALIAS:
				return getLinkAlias();
			case OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR:
				return getSuperior();
			case OrganizationDSLPackage.ORGANIZATION_POSITION__ROLES:
				return getRoles();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_POSITION__LINK_ALIAS:
				setLinkAlias((String)newValue);
				return;
			case OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR:
				setSuperior((OrganizationSuperior)newValue);
				return;
			case OrganizationDSLPackage.ORGANIZATION_POSITION__ROLES:
				getRoles().clear();
				getRoles().addAll((Collection<? extends OrganizationRole>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_POSITION__LINK_ALIAS:
				setLinkAlias(LINK_ALIAS_EDEFAULT);
				return;
			case OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR:
				setSuperior((OrganizationSuperior)null);
				return;
			case OrganizationDSLPackage.ORGANIZATION_POSITION__ROLES:
				getRoles().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION_POSITION__LINK_ALIAS:
				return LINK_ALIAS_EDEFAULT == null ? linkAlias != null : !LINK_ALIAS_EDEFAULT.equals(linkAlias);
			case OrganizationDSLPackage.ORGANIZATION_POSITION__SUPERIOR:
				return superior != null;
			case OrganizationDSLPackage.ORGANIZATION_POSITION__ROLES:
				return roles != null && !roles.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (linkAlias: ");
		result.append(linkAlias);
		result.append(')');
		return result.toString();
	}

} //OrganizationPositionImpl
