/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.action.ActionToolbar;

import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Organization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl#getSuperiorPosValue <em>Superior Pos Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl#getToolbar <em>Toolbar</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl#getOrganizationPositions <em>Organization Positions</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationImpl#getSubOrganizations <em>Sub Organizations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OrganizationImpl extends OrganizationBaseImpl implements Organization {
	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuperiorPosValue() <em>Superior Pos Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperiorPosValue()
	 * @generated
	 * @ordered
	 */
	protected OrganizationPosition superiorPosValue;

	/**
	 * The cached value of the '{@link #getToolbar() <em>Toolbar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToolbar()
	 * @generated
	 * @ordered
	 */
	protected ActionToolbar toolbar;

	/**
	 * The cached value of the '{@link #getOrganizationPositions() <em>Organization Positions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrganizationPositions()
	 * @generated
	 * @ordered
	 */
	protected EList<OrganizationPosition> organizationPositions;

	/**
	 * The cached value of the '{@link #getSubOrganizations() <em>Sub Organizations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubOrganizations()
	 * @generated
	 * @ordered
	 */
	protected EList<Organization> subOrganizations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrganizationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganizationDSLPackage.Literals.ORGANIZATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganizationDSLPackage.ORGANIZATION__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganizationPosition getSuperiorPosValue() {
		if (superiorPosValue != null && superiorPosValue.eIsProxy()) {
			InternalEObject oldSuperiorPosValue = (InternalEObject)superiorPosValue;
			superiorPosValue = (OrganizationPosition)eResolveProxy(oldSuperiorPosValue);
			if (superiorPosValue != oldSuperiorPosValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganizationDSLPackage.ORGANIZATION__SUPERIOR_POS_VALUE, oldSuperiorPosValue, superiorPosValue));
			}
		}
		return superiorPosValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganizationPosition basicGetSuperiorPosValue() {
		return superiorPosValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperiorPosValue(OrganizationPosition newSuperiorPosValue) {
		OrganizationPosition oldSuperiorPosValue = superiorPosValue;
		superiorPosValue = newSuperiorPosValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganizationDSLPackage.ORGANIZATION__SUPERIOR_POS_VALUE, oldSuperiorPosValue, superiorPosValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionToolbar getToolbar() {
		if (toolbar != null && toolbar.eIsProxy()) {
			InternalEObject oldToolbar = (InternalEObject)toolbar;
			toolbar = (ActionToolbar)eResolveProxy(oldToolbar);
			if (toolbar != oldToolbar) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganizationDSLPackage.ORGANIZATION__TOOLBAR, oldToolbar, toolbar));
			}
		}
		return toolbar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionToolbar basicGetToolbar() {
		return toolbar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToolbar(ActionToolbar newToolbar) {
		ActionToolbar oldToolbar = toolbar;
		toolbar = newToolbar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganizationDSLPackage.ORGANIZATION__TOOLBAR, oldToolbar, toolbar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrganizationPosition> getOrganizationPositions() {
		if (organizationPositions == null) {
			organizationPositions = new EObjectContainmentEList<OrganizationPosition>(OrganizationPosition.class, this, OrganizationDSLPackage.ORGANIZATION__ORGANIZATION_POSITIONS);
		}
		return organizationPositions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Organization> getSubOrganizations() {
		if (subOrganizations == null) {
			subOrganizations = new EObjectContainmentEList<Organization>(Organization.class, this, OrganizationDSLPackage.ORGANIZATION__SUB_ORGANIZATIONS);
		}
		return subOrganizations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION__ORGANIZATION_POSITIONS:
				return ((InternalEList<?>)getOrganizationPositions()).basicRemove(otherEnd, msgs);
			case OrganizationDSLPackage.ORGANIZATION__SUB_ORGANIZATIONS:
				return ((InternalEList<?>)getSubOrganizations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION__TITLE:
				return getTitle();
			case OrganizationDSLPackage.ORGANIZATION__SUPERIOR_POS_VALUE:
				if (resolve) return getSuperiorPosValue();
				return basicGetSuperiorPosValue();
			case OrganizationDSLPackage.ORGANIZATION__TOOLBAR:
				if (resolve) return getToolbar();
				return basicGetToolbar();
			case OrganizationDSLPackage.ORGANIZATION__ORGANIZATION_POSITIONS:
				return getOrganizationPositions();
			case OrganizationDSLPackage.ORGANIZATION__SUB_ORGANIZATIONS:
				return getSubOrganizations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION__TITLE:
				setTitle((String)newValue);
				return;
			case OrganizationDSLPackage.ORGANIZATION__SUPERIOR_POS_VALUE:
				setSuperiorPosValue((OrganizationPosition)newValue);
				return;
			case OrganizationDSLPackage.ORGANIZATION__TOOLBAR:
				setToolbar((ActionToolbar)newValue);
				return;
			case OrganizationDSLPackage.ORGANIZATION__ORGANIZATION_POSITIONS:
				getOrganizationPositions().clear();
				getOrganizationPositions().addAll((Collection<? extends OrganizationPosition>)newValue);
				return;
			case OrganizationDSLPackage.ORGANIZATION__SUB_ORGANIZATIONS:
				getSubOrganizations().clear();
				getSubOrganizations().addAll((Collection<? extends Organization>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case OrganizationDSLPackage.ORGANIZATION__SUPERIOR_POS_VALUE:
				setSuperiorPosValue((OrganizationPosition)null);
				return;
			case OrganizationDSLPackage.ORGANIZATION__TOOLBAR:
				setToolbar((ActionToolbar)null);
				return;
			case OrganizationDSLPackage.ORGANIZATION__ORGANIZATION_POSITIONS:
				getOrganizationPositions().clear();
				return;
			case OrganizationDSLPackage.ORGANIZATION__SUB_ORGANIZATIONS:
				getSubOrganizations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganizationDSLPackage.ORGANIZATION__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case OrganizationDSLPackage.ORGANIZATION__SUPERIOR_POS_VALUE:
				return superiorPosValue != null;
			case OrganizationDSLPackage.ORGANIZATION__TOOLBAR:
				return toolbar != null;
			case OrganizationDSLPackage.ORGANIZATION__ORGANIZATION_POSITIONS:
				return organizationPositions != null && !organizationPositions.isEmpty();
			case OrganizationDSLPackage.ORGANIZATION__SUB_ORGANIZATIONS:
				return subOrganizations != null && !subOrganizations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (title: ");
		result.append(title);
		result.append(')');
		return result.toString();
	}

} //OrganizationImpl
