/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.xtext.action.ActionDSLPackage;

import org.eclipse.osbp.xtext.authorizationdsl.AuthorizationDSLPackage;

import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationBase;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLFactory;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationLazyResolver;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationModel;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationRole;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior;

import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OrganizationDSLPackageImpl extends EPackageImpl implements OrganizationDSLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationLazyResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationPositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationSuperiorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass organizationRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OrganizationDSLPackageImpl() {
		super(eNS_URI, OrganizationDSLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OrganizationDSLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OrganizationDSLPackage init() {
		if (isInited) return (OrganizationDSLPackage)EPackage.Registry.INSTANCE.getEPackage(OrganizationDSLPackage.eNS_URI);

		// Obtain or create and register package
		OrganizationDSLPackageImpl theOrganizationDSLPackage = (OrganizationDSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OrganizationDSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OrganizationDSLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ActionDSLPackage.eINSTANCE.eClass();
		AuthorizationDSLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOrganizationDSLPackage.createPackageContents();

		// Initialize created meta-data
		theOrganizationDSLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOrganizationDSLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OrganizationDSLPackage.eNS_URI, theOrganizationDSLPackage);
		return theOrganizationDSLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganizationModel() {
		return organizationModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganizationModel_ImportSection() {
		return (EReference)organizationModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganizationModel_Pckg() {
		return (EReference)organizationModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganizationLazyResolver() {
		return organizationLazyResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOrganizationLazyResolver__EResolveProxy__InternalEObject() {
		return organizationLazyResolverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganizationPackage() {
		return organizationPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganizationPackage_Organizations() {
		return (EReference)organizationPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganizationBase() {
		return organizationBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOrganizationBase_Name() {
		return (EAttribute)organizationBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganization() {
		return organizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOrganization_Title() {
		return (EAttribute)organizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganization_SuperiorPosValue() {
		return (EReference)organizationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganization_Toolbar() {
		return (EReference)organizationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganization_OrganizationPositions() {
		return (EReference)organizationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganization_SubOrganizations() {
		return (EReference)organizationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganizationPosition() {
		return organizationPositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOrganizationPosition_LinkAlias() {
		return (EAttribute)organizationPositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganizationPosition_Superior() {
		return (EReference)organizationPositionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganizationPosition_Roles() {
		return (EReference)organizationPositionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganizationSuperior() {
		return organizationSuperiorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganizationSuperior_SuperiorPosValue() {
		return (EReference)organizationSuperiorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrganizationRole() {
		return organizationRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOrganizationRole_Role() {
		return (EReference)organizationRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganizationDSLFactory getOrganizationDSLFactory() {
		return (OrganizationDSLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		organizationModelEClass = createEClass(ORGANIZATION_MODEL);
		createEReference(organizationModelEClass, ORGANIZATION_MODEL__IMPORT_SECTION);
		createEReference(organizationModelEClass, ORGANIZATION_MODEL__PCKG);

		organizationLazyResolverEClass = createEClass(ORGANIZATION_LAZY_RESOLVER);
		createEOperation(organizationLazyResolverEClass, ORGANIZATION_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT);

		organizationPackageEClass = createEClass(ORGANIZATION_PACKAGE);
		createEReference(organizationPackageEClass, ORGANIZATION_PACKAGE__ORGANIZATIONS);

		organizationBaseEClass = createEClass(ORGANIZATION_BASE);
		createEAttribute(organizationBaseEClass, ORGANIZATION_BASE__NAME);

		organizationEClass = createEClass(ORGANIZATION);
		createEAttribute(organizationEClass, ORGANIZATION__TITLE);
		createEReference(organizationEClass, ORGANIZATION__SUPERIOR_POS_VALUE);
		createEReference(organizationEClass, ORGANIZATION__TOOLBAR);
		createEReference(organizationEClass, ORGANIZATION__ORGANIZATION_POSITIONS);
		createEReference(organizationEClass, ORGANIZATION__SUB_ORGANIZATIONS);

		organizationPositionEClass = createEClass(ORGANIZATION_POSITION);
		createEAttribute(organizationPositionEClass, ORGANIZATION_POSITION__LINK_ALIAS);
		createEReference(organizationPositionEClass, ORGANIZATION_POSITION__SUPERIOR);
		createEReference(organizationPositionEClass, ORGANIZATION_POSITION__ROLES);

		organizationSuperiorEClass = createEClass(ORGANIZATION_SUPERIOR);
		createEReference(organizationSuperiorEClass, ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE);

		organizationRoleEClass = createEClass(ORGANIZATION_ROLE);
		createEReference(organizationRoleEClass, ORGANIZATION_ROLE__ROLE);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		ActionDSLPackage theActionDSLPackage = (ActionDSLPackage)EPackage.Registry.INSTANCE.getEPackage(ActionDSLPackage.eNS_URI);
		AuthorizationDSLPackage theAuthorizationDSLPackage = (AuthorizationDSLPackage)EPackage.Registry.INSTANCE.getEPackage(AuthorizationDSLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		organizationPackageEClass.getESuperTypes().add(theOSBPTypesPackage.getLPackage());
		organizationBaseEClass.getESuperTypes().add(this.getOrganizationLazyResolver());
		organizationEClass.getESuperTypes().add(this.getOrganizationBase());
		organizationPositionEClass.getESuperTypes().add(this.getOrganizationBase());
		organizationSuperiorEClass.getESuperTypes().add(this.getOrganizationLazyResolver());
		organizationRoleEClass.getESuperTypes().add(this.getOrganizationLazyResolver());

		// Initialize classes, features, and operations; add parameters
		initEClass(organizationModelEClass, OrganizationModel.class, "OrganizationModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOrganizationModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, OrganizationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOrganizationModel_Pckg(), this.getOrganizationPackage(), null, "pckg", null, 0, 1, OrganizationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(organizationLazyResolverEClass, OrganizationLazyResolver.class, "OrganizationLazyResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getOrganizationLazyResolver__EResolveProxy__InternalEObject(), theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(organizationPackageEClass, OrganizationPackage.class, "OrganizationPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOrganizationPackage_Organizations(), this.getOrganization(), null, "organizations", null, 0, -1, OrganizationPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(organizationBaseEClass, OrganizationBase.class, "OrganizationBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOrganizationBase_Name(), theEcorePackage.getEString(), "name", null, 0, 1, OrganizationBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(organizationEClass, Organization.class, "Organization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOrganization_Title(), theEcorePackage.getEString(), "title", null, 0, 1, Organization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOrganization_SuperiorPosValue(), this.getOrganizationPosition(), null, "superiorPosValue", null, 0, 1, Organization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOrganization_Toolbar(), theActionDSLPackage.getActionToolbar(), null, "toolbar", null, 0, 1, Organization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOrganization_OrganizationPositions(), this.getOrganizationPosition(), null, "organizationPositions", null, 0, -1, Organization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOrganization_SubOrganizations(), this.getOrganization(), null, "subOrganizations", null, 0, -1, Organization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(organizationPositionEClass, OrganizationPosition.class, "OrganizationPosition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOrganizationPosition_LinkAlias(), theEcorePackage.getEString(), "linkAlias", null, 0, 1, OrganizationPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOrganizationPosition_Superior(), this.getOrganizationSuperior(), null, "superior", null, 0, 1, OrganizationPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOrganizationPosition_Roles(), this.getOrganizationRole(), null, "roles", null, 0, -1, OrganizationPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(organizationSuperiorEClass, OrganizationSuperior.class, "OrganizationSuperior", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOrganizationSuperior_SuperiorPosValue(), this.getOrganizationPosition(), null, "superiorPosValue", null, 0, 1, OrganizationSuperior.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(organizationRoleEClass, OrganizationRole.class, "OrganizationRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOrganizationRole_Role(), theAuthorizationDSLPackage.getRole(), null, "role", null, 0, 1, OrganizationRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "organizationDSL"
		   });
	}

} //OrganizationDSLPackageImpl
