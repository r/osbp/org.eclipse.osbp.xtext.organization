/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage
 * @generated
 */
public interface OrganizationDSLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OrganizationDSLFactory eINSTANCE = org.eclipse.osbp.xtext.organizationdsl.impl.OrganizationDSLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Organization Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization Model</em>'.
	 * @generated
	 */
	OrganizationModel createOrganizationModel();

	/**
	 * Returns a new object of class '<em>Organization Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization Lazy Resolver</em>'.
	 * @generated
	 */
	OrganizationLazyResolver createOrganizationLazyResolver();

	/**
	 * Returns a new object of class '<em>Organization Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization Package</em>'.
	 * @generated
	 */
	OrganizationPackage createOrganizationPackage();

	/**
	 * Returns a new object of class '<em>Organization Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization Base</em>'.
	 * @generated
	 */
	OrganizationBase createOrganizationBase();

	/**
	 * Returns a new object of class '<em>Organization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization</em>'.
	 * @generated
	 */
	Organization createOrganization();

	/**
	 * Returns a new object of class '<em>Organization Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization Position</em>'.
	 * @generated
	 */
	OrganizationPosition createOrganizationPosition();

	/**
	 * Returns a new object of class '<em>Organization Superior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization Superior</em>'.
	 * @generated
	 */
	OrganizationSuperior createOrganizationSuperior();

	/**
	 * Returns a new object of class '<em>Organization Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Organization Role</em>'.
	 * @generated
	 */
	OrganizationRole createOrganizationRole();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OrganizationDSLPackage getOrganizationDSLPackage();

} //OrganizationDSLFactory
