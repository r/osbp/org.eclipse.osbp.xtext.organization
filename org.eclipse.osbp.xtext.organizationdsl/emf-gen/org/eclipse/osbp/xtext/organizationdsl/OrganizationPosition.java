/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Jose C. Dominguez - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.organizationdsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Organization Position</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getLinkAlias <em>Link Alias</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getSuperior <em>Superior</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getRoles <em>Roles</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganizationPosition()
 * @model
 * @generated
 */
public interface OrganizationPosition extends OrganizationBase {
	/**
	 * Returns the value of the '<em><b>Link Alias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Alias</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Alias</em>' attribute.
	 * @see #setLinkAlias(String)
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganizationPosition_LinkAlias()
	 * @model unique="false"
	 * @generated
	 */
	String getLinkAlias();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getLinkAlias <em>Link Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link Alias</em>' attribute.
	 * @see #getLinkAlias()
	 * @generated
	 */
	void setLinkAlias(String value);

	/**
	 * Returns the value of the '<em><b>Superior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Superior</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superior</em>' containment reference.
	 * @see #setSuperior(OrganizationSuperior)
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganizationPosition_Superior()
	 * @model containment="true"
	 * @generated
	 */
	OrganizationSuperior getSuperior();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition#getSuperior <em>Superior</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Superior</em>' containment reference.
	 * @see #getSuperior()
	 * @generated
	 */
	void setSuperior(OrganizationSuperior value);

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.organizationdsl.OrganizationRole}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage#getOrganizationPosition_Roles()
	 * @model containment="true"
	 * @generated
	 */
	EList<OrganizationRole> getRoles();

} // OrganizationPosition
