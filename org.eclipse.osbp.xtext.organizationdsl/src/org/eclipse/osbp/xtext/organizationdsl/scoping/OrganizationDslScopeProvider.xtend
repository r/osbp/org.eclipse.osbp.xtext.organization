/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.organizationdsl.scoping

import javax.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.osbp.xtext.datamartdsl.jvmmodel.DatamartDSLJvmModelInferrer
import org.eclipse.osbp.xtext.organizationdsl.Organization
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDslUtils
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.xbase.annotations.typesystem.XbaseWithAnnotationsBatchScopeProvider

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 *
 */
class OrganizationDslScopeProvider extends AbstractOrganizationDslScopeProvider {
	
	@Inject extension DatamartDSLJvmModelInferrer datamartInferrer
	@Inject extension OrganizationDslUtils orgUtils
	
	@Override
	override IScope getScope(EObject context, EReference reference) {
		if (reference == OrganizationDSLPackage.Literals.ORGANIZATION_SUPERIOR__SUPERIOR_POS_VALUE) {
			return getScope_Organization_Superior_Superior(context as OrganizationSuperior)
		} else if (reference == OrganizationDSLPackage.Literals.ORGANIZATION__SUPERIOR_POS_VALUE) {
			return getScope_Organization_SuperiorPosValue(context as Organization)
		} else {
			return super.getScope(context, reference)
		}
	}
	
	def getScope_Organization_SuperiorPosValue(Organization organization) {
		var result = <EObject>newArrayList
		var eObj = organization.eContainer()
		while (!(eObj instanceof Organization)) {
			eObj = eObj.eContainer()
		}
		if (eObj != null) {
			for (OrganizationPosition pos : (eObj as Organization).getOrganizationPositions) {
				result.add(pos)
			}
			return Scopes.scopeFor(result)
		}
	}
	
	def getScope_Organization_Superior_Superior(OrganizationSuperior orgSup) {
		var result = <EObject>newArrayList
		var OrganizationPosition orgSupPosition
		var eObj = orgSup.eContainer()
		while (!(eObj instanceof Organization)) {
			if (eObj instanceof OrganizationPosition){
				orgSupPosition = (eObj as OrganizationPosition)
			}
			eObj = eObj.eContainer()
		}
		if (eObj != null) {
			for (OrganizationPosition pos : (eObj as Organization).getOrganizationPositions) {
				if (pos.name!=null && orgSupPosition!=null && !pos.name.equals(orgSupPosition.name))
				result.add(pos)
			}
			return Scopes.scopeFor(result)
		}
	}
}
