/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.organizationdsl

import javax.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.naming.IQualifiedNameProvider

class OrganizationDslUtils {
	
	@Inject extension IQualifiedNameProvider
	
	public final val MX_GRAPH_PREFIX = "MxGraph"
	public final val ORG_TXT = "Organization"
	public final val JS_TXT = "Js"
	public final val STATE_SUFFIX = "State"
	public final val ORG_SUFFIX = "Organization"
	public final val POS_SUFFIX = "Position"
	public final val MX_GRAPH_ORG_CHART_VIEW_ID = '''«MX_GRAPH_PREFIX»«ORG_TXT»'''
	public final val MX_GRAPH_ORG_CHART_ID = '''«MX_GRAPH_PREFIX»«ORG_TXT»«JS_TXT»'''
	public final val MX_GRAPH_ORG_CHART_STATE_ID = '''«MX_GRAPH_PREFIX»«ORG_TXT»«JS_TXT»«STATE_SUFFIX»'''
	
 	def i18nKey(EObject obj){
		return obj.fullyQualifiedName.toString
	}
	private def String createFilename(Organization organization){
		return organization?.name?.toString
	}
	
	def String createJsChartFilename(Organization organization){
		return organization?.createFilename?.toFirstUpper.concat(MX_GRAPH_ORG_CHART_ID)
	}
	
	def String createfullyQualifiedJsChartFilename(Organization organization){
		return organization?.fullyQualifiedNameToFirstUpper?.toString?.concat(MX_GRAPH_ORG_CHART_ID)
	}
	
	def String createJsFilename(Organization organization){
		return organization?.createFilename?.concat("MxGraphOrg.js")
	}
	
	def String createJClassJsFilename(Organization organization){
		return organization?.createFilename?.concat("MxGraphOrgJClass.js")
	}
	
	def String createfullyQualifiedJsStateFilename(Organization organization){
		return organization?.fullyQualifiedNameToFirstUpper?.toString?.concat(MX_GRAPH_ORG_CHART_STATE_ID)
	}
	
	def String createStateFilename(Organization organization){
		return organization?.createFilename?.toFirstUpper.concat(MX_GRAPH_ORG_CHART_STATE_ID)
	}
	
	def String createViewFilename(Organization organization){
		return organization?.createFilename?.toFirstUpper.concat(MX_GRAPH_ORG_CHART_VIEW_ID)
	}

	def String createResourceBundlePath(Organization organization){
		return organization?.createFilename?.concat("ResourceBundle")
	}
	
	def String createResBundleFilename(Organization organization){
		return organization?.createFilename?.concat("ResourceBundle.properties")
	}
	
	def String createOrganizationFilename(Organization organization){
		return organization?.name.toFirstUpper.concat(ORG_SUFFIX)
	}
	
	def String createPositionFilename(OrganizationPosition position){
		return position.name.toFirstUpper.concat(POS_SUFFIX)
	}
	
	def String fullyQualifiedNameToFirstUpper(Organization org){
		var fqn = org.fullyQualifiedName?.toString
		var lastIdxPoint = fqn.lastIndexOf('.')
		if (lastIdxPoint > 0){
			var firstPartStr = fqn.substring(0, lastIdxPoint)
			var orgName = fqn.substring(lastIdxPoint+1)
			var orgNameFU = orgName.toFirstUpper
			fqn = '''«firstPartStr».«orgNameFU»'''
		} else {
			fqn = org?.createFilename.toFirstUpper
		}
		fqn
	}
	
}
