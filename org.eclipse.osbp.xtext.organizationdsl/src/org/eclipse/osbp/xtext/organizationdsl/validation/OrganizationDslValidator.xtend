/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.organizationdsl.validation

import org.eclipse.osbp.xtext.organizationdsl.Organization
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition
import org.eclipse.xtext.validation.Check
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLPackage
import java.util.ArrayList

/**
 * Custom validation rules. 
 *
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class OrganizationDslValidator extends AbstractOrganizationDslValidator {

	/**
	 * The first position is not allowed to have a superior position (superiorPos)
	 */
	@Check
	def checkOrganizationFirstPositionSuperiorPos(Organization org) {
		// Check that the first organization position has no reference to a superior position
		if(!org.organizationPositions.empty) {
			val organizationPosition = org.organizationPositions.get(0)
			if (organizationPosition!=null){
				val superior = organizationPosition.getSuperior
				if (superior != null && superior.superiorPosValue != null) {
					error('First position only without superiorPos reference allowed', organizationPosition, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR)
				}
				
			}
		}
	}
	
	@Check
	def void checkOrganizationNamesAndSuperiorPos(Organization org) {
		// Check that only one organization position has no reference to a superior position
		org.checkOrganizationNamesAndSuperiorPos(newArrayList())
	}

	/**
	 * validation that only one position exist without a superPos reference within each organisation
	 * and that no duplicate organization name is used.
	 */
	private def checkOrganizationPositionNamesAndSuperiorPos(Organization org) {
		// Check that only one organization position has no reference to a superior position
		var posNames = newArrayList()
		var superiorPos = 0
		for (OrganizationPosition pos : org.organizationPositions) {
			if (posNames.contains(pos.getName)){
				error('Duplicate position name: '.concat(pos.getName),pos,OrganizationDSLPackage.Literals.ORGANIZATION_BASE__NAME)
			} else {
				posNames.add(pos.getName)
			}
			val superior = pos.getSuperior
			// 
			if (superior != null){
				if (superior.superiorPosValue == null){
					error('Please include superiorPos value', pos, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR)
				} else if (pos.name!=null && pos.name.equals(superior.superiorPosValue.name)) {
					error('Please do not use the same position as superiorPos (endless loop)', pos, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR)
				}
			}
			if (superior == null || (superior != null && superior.superiorPosValue == null)) {
				if (superiorPos > 0){
					error('Only one position without superiorPos reference allowed', pos, OrganizationDSLPackage.Literals.ORGANIZATION_POSITION__SUPERIOR)
				} else {
					superiorPos = superiorPos + 1
				}
			}
		}
	}
	
	
	/**
	 * validation that only one position exist without a superior position (superiorPos) reference within each organization
	 * and that no duplicate organization name is used.
	 */
	private def void checkOrganizationNamesAndSuperiorPos(Organization org, ArrayList<String> orgNames){
		// Necessary due to superiorPos is final
		if (org != null) {
			if (orgNames.contains(org.getName)){
				error('Duplicate organization name: '.concat(org.getName),org,OrganizationDSLPackage.Literals.ORGANIZATION_BASE__NAME)
			} else {
				orgNames.add(org.getName)
			}
			org.checkOrganizationPositionNamesAndSuperiorPos
			for (Organization subOrg : org.subOrganizations) {
				subOrg.checkOrganizationNamesAndSuperiorPos(orgNames)
			}
		}
	}
}

