/******************************************************************
 *                                                                *
 *  Copyright (C) - Loetz GmbH & Co KG, 69115 Heidelberg, Germany *
 *                                                                *
 *  All rights reserved. This program and the accompanying        *
 *  materials is NOT public domain and cannot be distributed      *
 *  without explicit permission.                                  *
 *                                                                *
 *================================================================*
 *                                                                *
 *  @file           $HeadURL$                                     *
 *  @version        $Revision$                                    *
 *  @date           $Date$                                        *
 *  @author         $Author$                                      *
 *                                                                *
 ******************************************************************/
 package org.eclipse.osbp.xtext.organizationdsl.jvmmodel

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator
import org.eclipse.xtext.generator.IFileSystemAccess

class OrganizationModelGenerator extends I18NModelGenerator {

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		super.doGenerate(input, fsa)
	}
}