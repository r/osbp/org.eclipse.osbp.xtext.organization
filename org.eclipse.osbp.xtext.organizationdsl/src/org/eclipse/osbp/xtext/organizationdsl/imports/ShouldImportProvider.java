package org.eclipse.osbp.xtext.organizationdsl.imports;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.xtext.action.ActionDSLPackage;
import org.eclipse.osbp.xtext.action.ActionToolbar;
import org.eclipse.osbp.xtext.authorizationdsl.AuthorizationDSLPackage;
import org.eclipse.osbp.xtext.authorizationdsl.Role;
import org.eclipse.osbp.xtext.oxtype.imports.DefaultShouldImportProvider;

public class ShouldImportProvider extends DefaultShouldImportProvider {

	protected boolean doShouldImport(EObject toImport, EReference eRef, EObject context) {
		return toImport instanceof Role ||
			toImport instanceof ActionToolbar;
	}

	protected boolean doShouldProposeAllElements(EObject object, EReference reference) {
		EClass type = reference.getEReferenceType();
		return AuthorizationDSLPackage.Literals.ROLE.isSuperTypeOf(type) || 
			ActionDSLPackage.Literals.ACTION_TOOLBAR.isSuperTypeOf(type);
	}
}
