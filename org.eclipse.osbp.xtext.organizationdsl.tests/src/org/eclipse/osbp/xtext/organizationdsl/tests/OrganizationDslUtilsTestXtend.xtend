/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.organizationdsl.tests

import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLFactory
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDslUtils
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import com.google.inject.Inject

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(InjectorProvider))

class OrganizationDslUtilsTestXtend {
	
	@Inject extension OrganizationDslUtils
	
	@Test
	def void fqnTest(){
		var org = OrganizationDSLFactory.eINSTANCE.createOrganization()
		org.setName("myfirstorganization")
		var orgPckg = OrganizationDSLFactory.eINSTANCE.createOrganizationPackage()
		orgPckg.name = "org.osbp.xtext.organizationdsl"
		orgPckg.organizations += org
		var fqn = fullyQualifiedNameToFirstUpper(org)
		Assert.assertEquals("org.osbp.xtext.organizationdsl.Myfirstorganization", fqn)
	}
	
}
