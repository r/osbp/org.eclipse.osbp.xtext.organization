/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.organizationdsl.tests;

import org.eclipse.xtext.junit4.IInjectorProvider;
import org.eclipse.osbp.dsl.entity.xtext.EntityGrammarStandaloneSetup;

import com.google.inject.Injector;

public class InjectorProvider implements IInjectorProvider {

	@SuppressWarnings("restriction")
	@Override
	public Injector getInjector() {
		return new EntityGrammarStandaloneSetup()
				.createInjectorAndDoEMFRegistration();
	}

}
