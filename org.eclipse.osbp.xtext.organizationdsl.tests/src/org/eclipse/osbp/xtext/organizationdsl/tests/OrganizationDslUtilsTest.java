/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.organizationdsl.tests;

import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLFactory;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDslUtils;
import org.junit.Assert;
import org.junit.Test;

public class OrganizationDslUtilsTest {
	
	@Test
	public void fqnTest(){
		OrganizationDslUtils utils = new OrganizationDslUtils();
		Organization org = OrganizationDSLFactory.eINSTANCE.createOrganization();
		org.setName("org.osbp.xtext.organizationdsl.myfirstorganization");
		String fqn = utils.fullyQualifiedNameToFirstUpper(org);
		Assert.assertEquals("org.osbp.xtext.organizationdsl.Myfirstorganization", fqn);
	}

}
