/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.organizationdsl.tests;

import com.google.inject.Inject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLFactory;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDslUtils;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage;
import org.eclipse.osbp.xtext.organizationdsl.tests.InjectorProvider;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(XtextRunner.class)
@InjectWith(InjectorProvider.class)
@SuppressWarnings("all")
public class OrganizationDslUtilsTestXtend {
  @Inject
  @Extension
  private OrganizationDslUtils _organizationDslUtils;
  
  @Test
  public void fqnTest() {
    Organization org = OrganizationDSLFactory.eINSTANCE.createOrganization();
    org.setName("myfirstorganization");
    OrganizationPackage orgPckg = OrganizationDSLFactory.eINSTANCE.createOrganizationPackage();
    orgPckg.setName("org.osbp.xtext.organizationdsl");
    EList<Organization> _organizations = orgPckg.getOrganizations();
    _organizations.add(org);
    String fqn = this._organizationDslUtils.fullyQualifiedNameToFirstUpper(org);
    Assert.assertEquals("org.osbp.xtext.organizationdsl.Myfirstorganization", fqn);
  }
}
