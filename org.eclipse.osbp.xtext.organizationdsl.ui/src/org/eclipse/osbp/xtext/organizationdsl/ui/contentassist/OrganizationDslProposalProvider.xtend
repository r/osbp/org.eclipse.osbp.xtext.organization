/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.organizationdsl.ui.contentassist

import javax.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper
import org.eclipse.osbp.xtext.organizationdsl.Organization
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior
import org.eclipse.osbp.xtext.organizationdsl.ui.OrganizationDSLDocumentationTranslator
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class OrganizationDslProposalProvider extends AbstractOrganizationDslProposalProvider {
	@Inject TerminalsProposalProvider provider
	@Inject BasicDSLProposalProviderHelper providerHelper

	override protected StyledString getKeywordDisplayString(Keyword keyword) {
		return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword,
			OrganizationDSLDocumentationTranslator.instance())
	}

	/**
	 * This override will enable 1 length non letter characters as keyword.
	 */
	override protected boolean isKeywordWorthyToPropose(Keyword keyword) {
		return true;
	}

	override protected boolean isValidProposal(String proposal, String prefix, ContentAssistContext context) {
		if ("T".equals(proposal)) {
			return false;
		}
		var result = super.isValidProposal(proposal, prefix, context);
		if (context.getCurrentModel() instanceof OrganizationSuperior) {
			return true;
		} else if (context.getCurrentModel() instanceof OrganizationPosition) {
			if ("superiorPos=".equals(proposal)) {
				return isSuperiorPosValidProposal(context.currentModel as OrganizationPosition, proposal, result);
			}
			return result;
		}
		return result;
	}

	/**
	 * First organization position is not allowed to have a superior position (superiorPos)
	 */
	private def boolean isSuperiorPosValidProposal(OrganizationPosition position, String proposal, boolean result) {
		if (position !== null) {
			var eObj = position.eContainer
			while (!(eObj instanceof Organization)) {
				eObj = eObj.eContainer
			}
			var org = (eObj as Organization)
			val firstOrgPosition = org.organizationPositions.get(0)
			if (firstOrgPosition !== null && firstOrgPosition.name.equals(position.name)) {
				return false
			}
		}
		return result
	}

	override public void complete_QualifiedName(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}

	// ------------------------ delegates to TerminalsProposalProvider -----------------
	override public void complete_TRANSLATABLESTRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}

	override public void complete_STRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}

	override public void complete_ID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_ID(model, ruleCall, context, acceptor)
	}

	override public void complete_TRANSLATABLEID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_ID(model, ruleCall, context, acceptor)
	}

}
