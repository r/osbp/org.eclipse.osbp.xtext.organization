/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.organizationdsl.ui;

import org.eclipse.xtext.ui.editor.hover.html.IEObjectHoverDocumentationProvider;

import org.eclipse.osbp.xtext.basic.ui.BasicDSLEObjectHoverProvider;

public class OrganizationDSLEObjectHoverProvider extends BasicDSLEObjectHoverProvider {
	 
    private static OrganizationDSLEObjectHoverProvider INSTANCE;

    public static OrganizationDSLEObjectHoverProvider instance() {
        return INSTANCE;
    }

    public OrganizationDSLEObjectHoverProvider() {
        super();
        INSTANCE = this;
    }
    
    @Override
    public IEObjectHoverDocumentationProvider getDocumentationHoverProvider() {
        return OrganizationDSLEObjectHoverDocumentationProvider.instance();
    }
}
