/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.organizationdsl.ui.labeling

import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider
import org.eclipse.osbp.xtext.organizationdsl.Organization
import org.eclipse.osbp.xtext.organizationdsl.OrganizationModel
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition
import org.eclipse.osbp.xtext.organizationdsl.OrganizationRole
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class OrganizationDslLabelProvider extends BasicDSLLabelProvider {

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	 override text ( Object o ) {
		switch (o) {
			OrganizationPackage		:	generateText( o, 'package'     	, (o as OrganizationPackage).name )
			Organization			:	generateText( o, 'definition'  	, (o as Organization).name )
			OrganizationPosition	:	generateText( o, 'position'    	, (o as OrganizationPosition).name )
			OrganizationRole		:	generateText( o, 'role'    		, (o as OrganizationRole).role.name )
			default					:	super.text( o )
		}
	} 
	
	override image ( Object o ) {
		switch (o) {
			OrganizationModel		:	getInternalImage( 'model.png', class)
			OrganizationPackage		:	getInternalImage( 'package.gif', class)
			Organization			:	getInternalImage( 'dsl_organigram.png', class)
			OrganizationPosition	:	getInternalImage( 'position.png', class)
			OrganizationSuperior	:	getInternalImage( 'superior.png', class)
			OrganizationRole		:	getInternalImage( 'role.png', class)
			default					:	super.image( o )
		}
	}
}
