/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.organizationdsl.ui.labeling;

import com.google.inject.Inject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider;
import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationModel;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPackage;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationRole;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior;

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
@SuppressWarnings("all")
public class OrganizationDslLabelProvider extends BasicDSLLabelProvider {
  @Inject
  public OrganizationDslLabelProvider(final AdapterFactoryLabelProvider delegate) {
    super(delegate);
  }
  
  @Override
  public Object text(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof OrganizationPackage) {
      _matched=true;
      _switchResult = this.generateText(o, "package", ((OrganizationPackage) o).getName());
    }
    if (!_matched) {
      if (o instanceof Organization) {
        _matched=true;
        _switchResult = this.generateText(o, "definition", ((Organization) o).getName());
      }
    }
    if (!_matched) {
      if (o instanceof OrganizationPosition) {
        _matched=true;
        _switchResult = this.generateText(o, "position", ((OrganizationPosition) o).getName());
      }
    }
    if (!_matched) {
      if (o instanceof OrganizationRole) {
        _matched=true;
        _switchResult = this.generateText(o, "role", ((OrganizationRole) o).getRole().getName());
      }
    }
    if (!_matched) {
      _switchResult = super.text(o);
    }
    return _switchResult;
  }
  
  @Override
  public Object image(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof OrganizationModel) {
      _matched=true;
      _switchResult = this.getInternalImage("model.png", this.getClass());
    }
    if (!_matched) {
      if (o instanceof OrganizationPackage) {
        _matched=true;
        _switchResult = this.getInternalImage("package.gif", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof Organization) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_organigram.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof OrganizationPosition) {
        _matched=true;
        _switchResult = this.getInternalImage("position.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof OrganizationSuperior) {
        _matched=true;
        _switchResult = this.getInternalImage("superior.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof OrganizationRole) {
        _matched=true;
        _switchResult = this.getInternalImage("role.png", this.getClass());
      }
    }
    if (!_matched) {
      _switchResult = super.image(o);
    }
    return _switchResult;
  }
}
