/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.organizationdsl.ui.contentassist;

import javax.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper;
import org.eclipse.osbp.xtext.organizationdsl.Organization;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationPosition;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationSuperior;
import org.eclipse.osbp.xtext.organizationdsl.ui.OrganizationDSLDocumentationTranslator;
import org.eclipse.osbp.xtext.organizationdsl.ui.contentassist.AbstractOrganizationDslProposalProvider;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
@SuppressWarnings("all")
public class OrganizationDslProposalProvider extends AbstractOrganizationDslProposalProvider {
  @Inject
  private TerminalsProposalProvider provider;
  
  @Inject
  private BasicDSLProposalProviderHelper providerHelper;
  
  @Override
  protected StyledString getKeywordDisplayString(final Keyword keyword) {
    return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword, 
      OrganizationDSLDocumentationTranslator.instance());
  }
  
  /**
   * This override will enable 1 length non letter characters as keyword.
   */
  @Override
  protected boolean isKeywordWorthyToPropose(final Keyword keyword) {
    return true;
  }
  
  @Override
  protected boolean isValidProposal(final String proposal, final String prefix, final ContentAssistContext context) {
    boolean _equals = "T".equals(proposal);
    if (_equals) {
      return false;
    }
    boolean result = super.isValidProposal(proposal, prefix, context);
    EObject _currentModel = context.getCurrentModel();
    if ((_currentModel instanceof OrganizationSuperior)) {
      return true;
    } else {
      EObject _currentModel_1 = context.getCurrentModel();
      if ((_currentModel_1 instanceof OrganizationPosition)) {
        boolean _equals_1 = "superiorPos=".equals(proposal);
        if (_equals_1) {
          EObject _currentModel_2 = context.getCurrentModel();
          return this.isSuperiorPosValidProposal(((OrganizationPosition) _currentModel_2), proposal, result);
        }
        return result;
      }
    }
    return result;
  }
  
  /**
   * First organization position is not allowed to have a superior position (superiorPos)
   */
  private boolean isSuperiorPosValidProposal(final OrganizationPosition position, final String proposal, final boolean result) {
    if ((position != null)) {
      EObject eObj = position.eContainer();
      while ((!(eObj instanceof Organization))) {
        eObj = eObj.eContainer();
      }
      Organization org = ((Organization) eObj);
      final OrganizationPosition firstOrgPosition = org.getOrganizationPositions().get(0);
      if (((firstOrgPosition != null) && firstOrgPosition.getName().equals(position.getName()))) {
        return false;
      }
    }
    return result;
  }
  
  @Override
  public void complete_QualifiedName(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this);
  }
  
  @Override
  public void complete_TRANSLATABLESTRING(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_STRING(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_STRING(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_STRING(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_ID(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_ID(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_TRANSLATABLEID(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_ID(model, ruleCall, context, acceptor);
  }
}
